export { default as postUser } from "./postUser";
export { default as deleteUser } from "./deleteUser";
export { default as updateUser } from "./updateUser";
export { default as getUser } from "./getUser";
export { default as getUsers} from "./getUsers"
